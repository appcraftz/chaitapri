package com.appcraftz.chaitapri;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class Accompaniments extends AppCompatActivity {

  /*  ListView listAccompaniments;
    ListViewAdapter adapter;*/
    RecyclerView rvAccompaniments;
    ColdBeveragesAdaptor adaptor;
    ArrayList<FoodItem> accompanimentsItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_accompaniments);
    }


    @Override
    public void onStart() {
        super.onStart();
     /*   if (accompanimentsItems!=null)
        accompanimentsItems.clear();
*/
        rvAccompaniments = (RecyclerView) findViewById(R.id.rvAccompaniments);
        accompanimentsItems = GlobalData.accompanimentsItems;
        adaptor = new ColdBeveragesAdaptor(GlobalData.accompanimentsItems,Accompaniments.this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(Accompaniments.this,LinearLayoutManager.VERTICAL,false);
        rvAccompaniments.setLayoutManager(layoutManager);
        rvAccompaniments.setAdapter(adaptor);
        /*listAccompaniments = (ListView)findViewById(R.id.liAccompaniments);
        //accompanimentsItems = GlobalData.getAccompanimentsItems();


        adapter = new ListViewAdapter(GlobalData.accompanimentsItems,getApplicationContext());

        listAccompaniments.setAdapter(adapter);

        adapter.setOnDisplayTotalCountClick(new DisplayTotalCountClick() {
            @Override
            public void onDisplayTotalCountClicked(int i) {

            }
        });
        adapter.notifyDataSetChanged();*/


    }
}
