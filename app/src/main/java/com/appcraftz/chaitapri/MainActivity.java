package com.appcraftz.chaitapri;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

//    private static final int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
//    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2;
//    private static final int MY_PERMISSIONS_REQUEST_INTERNET = 3;
//    private static final int MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE = 4;
//    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 5;

    private SliderLayout mDemoSlider;

    TabLayout tabLayout;
    ViewPager pager;
    Button franchise;
    TextView headTag;
    ImageButton viewOrders;
    TextView itemCount;
  //  SimpleFragmentPagerAdapter adapter;
    RatingBar rateBar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GlobalData.clear();
        GlobalData.setUpLists();
        //new AppLocationService(MainActivity.this).getLocation(LocationManager.GPS_PROVIDER);
        init();



    }

    private void init() {

        findViewById(R.id.rlHotBeverage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_hot=new Intent(MainActivity.this,HotBeverages.class);
                startActivity(i_hot);
            }
        }); findViewById(R.id.rlColdBeverage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_cold=new Intent(MainActivity.this,ColdBeverages.class);
                startActivity(i_cold);
            }
        });findViewById(R.id.rlSnacks).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_snack=new Intent(MainActivity.this,Snacks.class);
                startActivity(i_snack);
            }
        }); findViewById(R.id.rlAccompaniments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_accomp=new Intent(MainActivity.this,Accompaniments.class);
                startActivity(i_accomp);
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermission();
        } else {
            //Log.e("Permissions dialog","Yup");
        }
        final NetworkStatus networkstatus = NetworkStatus.getInstance(MainActivity.this);


            ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(MainActivity.this);
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationProvider.getLastKnownLocation()
                    .subscribe(new Action1<Location>() {
                        @Override
                        public void call(Location location2) {
                            //distance = location.distanceTo(location2);
                            GlobalData.myLocation =location2;
                       //     Log.e("Latitude",""+location2.getLatitude());
                         //   Log.e("Longitude",""+location2.getLongitude());

                        }
                    });



        if (SPHelper.getSP(getApplicationContext(), "finish").equals("kn"))
            SPHelper.setSP(getApplicationContext(), "finish", "false");


       // rateBar = (RatingBar) findViewById(R.id.rateBar);
       // headTag = (TextView) findViewById(R.id.headTag);


        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
       // tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
       // pager = (ViewPager) findViewById(R.id.pager);


        franchise = (Button) findViewById(R.id.franchiseButton);

        viewOrders = (ImageButton) findViewById(R.id.viewOrders);
        itemCount = (TextView) findViewById(R.id.itemCount);
        globalCount();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
                file_maps.put("ChaiTapri1", R.drawable.img1);
                file_maps.put("ChaiTapri2", R.drawable.img2);
                file_maps.put("ChaiTapri3", R.drawable.img3);
                file_maps.put("ChaiTapri4", R.drawable.img4);

                for (String name : file_maps.keySet()) {
                    TextSliderView textSliderView = new TextSliderView(MainActivity.this);
                    // initialize a SliderLayout
                    textSliderView
                            .image(file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(MainActivity.this);

                    //add your extra information
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }
                mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                mDemoSlider.setDuration(4000);
                mDemoSlider.addOnPageChangeListener(MainActivity.this);

            }
        });

      //  adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
     //   pager.setAdapter(adapter);
        // Give the TabLayout the ViewPager

  /*      runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(pager);
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {

                        itemCount.setText(SPHelper.getSP(getApplicationContext(), "size"));
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }
                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
                for (int i = 0; i < tabLayout.getTabCount(); i++) {
                    //noinspection ConstantConditions
                    TextView tv = (TextView) LayoutInflater.from(MainActivity.this).inflate(R.layout.tab, null);
                    tv.setText(adapter.getPageTitle(i));
                    tabLayout.getTabAt(i).setCustomView(tv);
                }

            }
        });*/


        franchise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, Franchaise.class);
                startActivity(intent);

            }
        });


        viewOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if (networkstatus.isOnline()){
                  SPHelper.setSP(getApplicationContext(), "finish", "false");
                  Intent intent = new Intent(MainActivity.this, Orders.class);
                  startActivity(intent);
              }else  {
                  showMessageOKCancel("You Should be Connected to Internet ",new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                      }
                  });

                }


            }
        });


    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        mDemoSlider.stopAutoCycle();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
/*
    public void checkPermissions() {
//            -----------------------------------ACCESS_COARSE_LOCATION
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);
            }
        }//            -----------------------------------ACCESS_FINE_LOCATION
        else if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }

        //            -----------------------------------ACCESS_INTERNET
        else if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_INTERNET);
            }
        }

        //            -----------------------------------MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE
        else if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_NETWORK_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE);
            }
        }

        //            -----------------------------------ACCESS_COARSE_LOCATION

        else if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_INTERNET: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_ACCESS_NETWORK_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}*/


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Access Location");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Access Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Access Internet");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Access Network State");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("Access External Storage");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this,permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.INTERNET, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_NETWORK_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                    //insertDummyContact();
                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void globalCount(){
        if (Cart.size() != 0)
            itemCount.setText("" + Cart.size());
        else
            itemCount.setText("0");
    }

}
