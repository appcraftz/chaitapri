package com.appcraftz.chaitapri;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Franchaise extends AppCompatActivity {

    TextView fran;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_franchaise);

        fran =(TextView)findViewById(R.id.fran);

        Typeface tf = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/cup_and_talon.ttf");
        fran.setTypeface(tf);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //GlobalData.clear();
        //GlobalData.setUpLists();
    }
}
