package com.appcraftz.chaitapri;

import java.util.ArrayList;

/**
 * Created by appcraftz on 15/9/17.
 */

public interface DisplayQuantityClick {
    public void onDisplayquantityClick(ArrayList<FoodItem> foodItemArrayList);
}
