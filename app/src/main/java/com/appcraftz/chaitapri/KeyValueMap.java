package com.appcraftz.chaitapri;

/**
 * Created by appcraftz on 15/9/17.
 */

public class KeyValueMap {
    private String key;
    private String value;

    public KeyValueMap(String key,String value)
    {
        this.key = key;
        this.value = value;
    }
    public String getKey()
    {
        return key;
    }
    public String getValue()
    {
        return value;
    }
}

