package com.appcraftz.chaitapri;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by appcraftz on 15/9/17.
 */

public class SPHelper {

    public static final String MY_PREFS_NAME = "ChaiTapri";

    public static void setSP(Context context, String key, String value){
        SharedPreferences.Editor editor = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getSP(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        String value = prefs.getString(key, null);
        if (value != null) {
            return  value;
        }
        return "none";
    }

    public static String getAllSP(Context context){
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        Map<String, ?> allEntries = prefs.getAll();
        String str = "";
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            //Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
            str = str + "\n" +entry.getKey() + ": " + entry.getValue().toString();
        }
        return str;
    }

    public static void removeAllSP(Context context){
        SharedPreferences settings = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    public static void removeSP(Context context, String keyname){
        SharedPreferences settings = context.getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        settings.edit().remove(keyname).commit();
    }

    public static JSONObject getJSONFromString(String str) throws JSONException {
        String strJson = str;
        JSONObject jsonData = null;
        if(strJson != null){
            jsonData = new JSONObject(strJson);
        }
        return jsonData;
    }
}
