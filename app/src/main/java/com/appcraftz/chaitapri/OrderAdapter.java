package com.appcraftz.chaitapri;

import android.content.Context;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by appcraftz on 15/9/17.
 */

public class OrderAdapter extends BaseAdapter{

    ArrayList<FoodItem> foodItemArrayList;
    LayoutInflater inflater;

    Context context;
    OrderCancelClick orderCancelClick;
    DisplayQuantityClick displayQuantityClick;
    int tot;
    int quan;
    OrderAdapter(ArrayList<FoodItem> foodItems, Context context){
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.foodItemArrayList = foodItems;

    }
    @Override
    public int getCount() {
        if (foodItemArrayList!=null)
            return foodItemArrayList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        return foodItemArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = inflater.inflate(R.layout.orderitem,null);

        final int position = i;

        TextView ordName = (TextView) v.findViewById(R.id.ordItemName);
        TextView ordPrice = (TextView)v.findViewById(R.id.ordItemPrice);
        ImageButton increase = (ImageButton)v.findViewById(R.id.ordIncrease);
        ImageButton decrease = (ImageButton)v.findViewById(R.id.ordDecrease);
        ImageButton cancel = (ImageButton)v.findViewById(R.id.cancelOrder);

        final EditText total = (EditText) v.findViewById(R.id.ordTotal);
        final EditText quantity = (EditText)v.findViewById(R.id.quantity);

if (foodItemArrayList!=null)
        //quantity.setText("1");

//quantity.setText(""+ foodItemArrayList.get(i).setQuantity(1));

        quantity.setText(""+foodItemArrayList.get(i).getQuantity());
            quan =Integer.parseInt(quantity.getText().toString());
        tot =quan  * foodItemArrayList.get(i).getPrice();

        total.setText(""+tot);
        foodItemArrayList.get(i).setTotal(tot);
        foodItemArrayList.get(i).setQuantity(quan);

        if (displayQuantityClick!=null)
            displayQuantityClick.onDisplayquantityClick(foodItemArrayList);

        ordName.setText(foodItemArrayList.get(i).getName());
        ordPrice.setText(String.valueOf(foodItemArrayList.get(i).getPrice())+"₹");


        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int inc = Integer.parseInt(quantity.getText().toString());
                inc = inc+1;
                quantity.setText(""+inc);

                quan =Integer.parseInt(quantity.getText().toString());
                tot =quan  * foodItemArrayList.get(i).getPrice();
             //   Log.e("TotalInc",""+tot);

                total.setText(""+tot);
                foodItemArrayList.get(i).setQuantity(quan);
                foodItemArrayList.get(i).setTotal(tot);
                if (displayQuantityClick!=null)
                    displayQuantityClick.onDisplayquantityClick(foodItemArrayList);


            }
        });


        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int dec = Integer.parseInt(quantity.getText().toString());
                dec = dec-1;

                if (dec<0)
                    Toast.makeText(context, "Quantity should not be less than Zero...", Toast.LENGTH_SHORT).show();
                else
                    quantity.setText(""+dec);


                quan =Integer.parseInt(quantity.getText().toString());
                tot = quan * foodItemArrayList.get(i).getPrice();

             //   Log.e("TotalDec",""+tot);

                total.setText(""+tot);
                foodItemArrayList.get(i).setQuantity(quan);
                foodItemArrayList.get(i).setTotal(tot);
                if (displayQuantityClick!=null)
                    displayQuantityClick.onDisplayquantityClick(foodItemArrayList);


            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (orderCancelClick!=null)
                {
                 //   Log.e("Cart Item", "onClick: " +Cart.size());
                    orderCancelClick.onOrderCancelClick(foodItemArrayList.get(i));
                }

            }
        });



        return v;


    }


    public void setOnOrderCancelClick(OrderCancelClick orderCancelClick){
        this.orderCancelClick = orderCancelClick;
    }

    public void setOnDisplayQuantityClick(DisplayQuantityClick displayQuantityClick){
        this.displayQuantityClick = displayQuantityClick;
    }
}
