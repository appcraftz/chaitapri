package com.appcraftz.chaitapri;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by appcraftz on 14/9/17.
 */
public class MyTextView extends TextView {

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.DEFAULT;

            switch (getTypeface().getStyle()) {
                case Typeface.BOLD:
                    tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/cup_and_talon.ttf");
                    break;

                case Typeface.ITALIC:
                    tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/cup_and_talon.ttf");
                    break;

                case Typeface.BOLD_ITALIC:
                    tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/cup_and_talon.ttf");
                    break;

                default:
                    tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/cup_and_talon.ttf");
                    break;
            }

            setTypeface(tf);
        }
    }

}