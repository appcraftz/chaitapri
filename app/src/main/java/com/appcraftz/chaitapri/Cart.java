package com.appcraftz.chaitapri;

import java.util.ArrayList;

/**
 * Created by Almas on 10/31/2017.
 */

public class Cart {
    public static ArrayList<FoodItem> foodItems = new ArrayList<FoodItem>();

    public static void addToCart(FoodItem foodItem)
    {
        foodItems.add(foodItem);
    }

    public static boolean contains(FoodItem foodItem){
        return foodItems.contains(foodItem);
    }

    public static void removeFromCart(FoodItem foodItem){
        foodItems.remove(foodItem);
    }

    public static int size( ){
        return foodItems.size();
    }

    public static ArrayList<FoodItem> getCartItems(){
        return foodItems;
    }

    public static void ClearCart(){
        foodItems.clear();
    }

}
