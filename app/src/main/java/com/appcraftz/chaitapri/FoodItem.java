package com.appcraftz.chaitapri;

/**
 * Created by appcraftz on 15/9/17.
 */

public class FoodItem {

    String name;
    int price;
    boolean isOrdered;
    int quantity;
    int total;


    FoodItem(String name,int price){
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setOrdered(boolean ordered) {
        isOrdered = ordered;
    }
    public boolean getOrdered() {
        return isOrdered;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}

