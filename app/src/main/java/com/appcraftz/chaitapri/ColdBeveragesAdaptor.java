package com.appcraftz.chaitapri;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Almas on 10/30/2017.
 */

public class ColdBeveragesAdaptor extends RecyclerView.Adapter<ColdBeveragesAdaptor.MyviewHolder>{
    private final ArrayList<FoodItem> coldBeveragesItems;
    private final Context context;

    public ColdBeveragesAdaptor(ArrayList<FoodItem> coldBeveragesItems, Context context) {
        this.coldBeveragesItems = coldBeveragesItems;
        this.context = context;
    }

    @Override
    public ColdBeveragesAdaptor.MyviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MyviewHolder( LayoutInflater.from(context) .inflate(R.layout.list_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final ColdBeveragesAdaptor.MyviewHolder holder, final int position) {

        holder.tvItemName.setText(coldBeveragesItems.get(position).getName());
        holder.tvItemPrice.setText(String.valueOf(coldBeveragesItems.get(position).getPrice()));
        holder.imvAddToCart.setImageResource(R.drawable.red_cart);

        if (!Cart.contains(coldBeveragesItems.get(position))){
            holder.imvAddToCart.setImageResource(R.drawable.red_cart);
        }else {
            holder.imvAddToCart.setImageResource(R.drawable.green_redcart);
        }

        holder.imvAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!Cart.contains(coldBeveragesItems.get(position))){
                    FoodItem foodItem = coldBeveragesItems.get(position);
                    Toast.makeText(context, "added to Cart..", Toast.LENGTH_SHORT).show();
                    //Log.d("Cart", "onClick: " +Cart.size());
                   foodItem.setQuantity(1);
                    Cart.addToCart(foodItem);
                    holder.imvAddToCart.setImageResource(R.drawable.green_redcart);
                }
                else{
                    Toast.makeText(context, "Remove From Cart..", Toast.LENGTH_SHORT).show();
                    FoodItem foodItem = coldBeveragesItems.get(position);
                    Cart.removeFromCart(foodItem);
                    holder.imvAddToCart.setImageResource(R.drawable.red_cart);

                }

           }
        });
    }

    @Override
    public int getItemCount() {
        return coldBeveragesItems.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView tvItemPrice,tvItemName;
        ImageButton imvAddToCart;
        public MyviewHolder(View itemView) {
            super(itemView);
            tvItemPrice=(TextView)itemView.findViewById(R.id.tvItemPrice);
            tvItemName=(TextView)itemView.findViewById(R.id.tvItemName);
            imvAddToCart=(ImageButton)itemView.findViewById(R.id.addToCart);

        }
    }
}
