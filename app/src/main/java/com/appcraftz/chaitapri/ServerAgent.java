package com.appcraftz.chaitapri;

import android.content.Context;
//import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by appcraftz on 15/9/17.
 */

public class ServerAgent {
    private OkHttpClient httpClient;
    private Context con;


    public ServerAgent(Context con)
    {
        httpClient = new OkHttpClient();
        this.con = con;

    }

    //TODO: add server methods here
    public String sendToServer(ArrayList<KeyValueMap> list, String serviceType)
    {
        // String response="";

        try
        {
          //  //Log.e("Enterred in Try","Yup");
            RequestBody formBody;
            FormBody.Builder builder= new FormBody.Builder();
          //  builder.add("api_key", GlobalVariables.API_KEY);

            for(KeyValueMap value:list)
            {
                builder.add(value.getKey(),value
                        .getValue());

              //  //Log.e("KEY--",value.getKey());
                //Log.e("VALUE--",value.getValue());

            }
            formBody = builder.build();


            Request request = new Request.Builder()
                    .url(serviceType)
                    .post(formBody)
                    .build();


            Response response = httpClient.newCall(request).execute();
            String resp = response.body().string();

            //Log.e("RESP fro server",resp);

            return resp;

        }catch (Exception ex){
            ex.printStackTrace();

            if(ex.getClass() == SocketTimeoutException.class)
            {
                return "CONNECTION_ERROR";
            }
        }
        return "fail";

        // return response;
    }


   /* public String addGroupMembers(String groupId, ArrayList<Employee> sample) {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();

        keyValueMapArrayList.add(new KeyValueMap("group_id",groupId));

        //Log.e("Group Id",groupId);


        JSONArray employee_name = new JSONArray();
        JSONArray employee_number = new JSONArray();

        for (Employee item:sample){


            employee_name.put(item.getName());
            employee_number.put(item.getNumber());


        }


        String json = employee_name.toString();
        String json2 = employee_number.toString();

        keyValueMapArrayList.add(new KeyValueMap("employee_name",json));
        keyValueMapArrayList.add(new KeyValueMap("employee_number",json2));

        String resp =sendToServer(keyValueMapArrayList,Url.addGroupMembers);

        return resp;
    }
*/
    public String placeOrder(UserData data) {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        JSONArray order = new JSONArray();
        for (FoodItem item: Cart.getCartItems()){
            JSONObject object = new JSONObject();
            try {
                object.put("name",item.getName());
                object.put("price",item.getPrice());
                object.put("quantity",item.getQuantity());
                order.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject ordersObj = new JSONObject();
        try {
            ordersObj.put("cart", order);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        keyValueMapArrayList.add(new KeyValueMap("user_name",data.getName()));
        keyValueMapArrayList.add(new KeyValueMap("user_contact",data.getContact()));
        keyValueMapArrayList.add(new KeyValueMap("user_address",data.getAddress()));
        keyValueMapArrayList.add(new KeyValueMap("total",String.valueOf(data.getTotal())));

        keyValueMapArrayList.add(new KeyValueMap("order",ordersObj.toString()));
        keyValueMapArrayList.add(new KeyValueMap("order_id",String.valueOf(System.currentTimeMillis())));

        //Log.e("UName",data.getName());
        //Log.e("contact",data.getContact());
        //Log.e("address",data.getAddress());
        //Log.e("total",String.valueOf(data.getTotal()));

        String resp = sendToServer(keyValueMapArrayList,"http://development.appcraftz.com/chai_tapori/webservices/get_order.php");

        //Log.e("Resp",resp);


        //Log.e("Size",""+Cart.size());

      //  Cart.ClearCart();
       /* GlobalData.clear();
        GlobalData.flag =true;  */    //for clearing Fields of Listviews in Fragments
        //Log.e("Size",""+Cart.size());
        return resp;
    }
}
