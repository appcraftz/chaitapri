package com.appcraftz.chaitapri;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class Snacks extends AppCompatActivity {

/*
    ListView listSnacks;
*/
    ColdBeveragesAdaptor adaptor;
    RecyclerView rvSnacks;
    ArrayList<FoodItem> snacksItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_snacks);
    }

    @Override
    public void onStart() {
        super.onStart();
       /* if (snacksItems!=null)
        snacksItems.clear();
*/
        rvSnacks=(RecyclerView)findViewById(R.id.rvSnacks);
        snacksItems = GlobalData.snacksItems;
        adaptor = new ColdBeveragesAdaptor(GlobalData.snacksItems,Snacks.this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(Snacks.this,LinearLayoutManager.VERTICAL,false);
        rvSnacks.setLayoutManager(layoutManager);
        rvSnacks.setAdapter(adaptor);
     /*   listSnacks = (ListView) findViewById(R.id.liSnacks);
        //snacksItems = GlobalData.getSnacksItems();


        adapter = new ListViewAdapter(GlobalData.snacksItems,getApplicationContext());

        listSnacks.setAdapter(adapter);

        adapter.setOnDisplayTotalCountClick(new DisplayTotalCountClick() {
            @Override
            public void onDisplayTotalCountClicked(int i) {

            }
        });
        adapter.notifyDataSetChanged();*/

    }
}
