package com.appcraftz.chaitapri;

import android.app.Application;

/**
 * Created by appcraftz on 13/9/17.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(),"SERIF","fonts/cup_and_talon.ttf");
    }
}
