package com.appcraftz.chaitapri;

import android.location.Location;

import java.util.ArrayList;

/**
 * Created by appcraftz on 15/9/17.
 */

public class GlobalData {

    public static Location myLocation;
    public static int globalCount;
    public static boolean flag = false;
    public static ArrayList<FoodItem> foodItems = new ArrayList<FoodItem>();

    public static ArrayList<FoodItem> coldBeveragesItems = new ArrayList<FoodItem>();
    public static ArrayList<FoodItem> hotBeveragesItems = new ArrayList<FoodItem>();
    public static ArrayList<FoodItem> accompanimentsItems = new ArrayList<FoodItem>();
    public static ArrayList<FoodItem> snacksItems = new ArrayList<FoodItem>();


    public static void setUpLists(){
        coldBeveragesItems.add(new FoodItem("Lemon Iced Chai",40));
        coldBeveragesItems.add(new FoodItem("Cold Coffee",40));
        coldBeveragesItems.add(new FoodItem("Nimbu Pani",20));
        coldBeveragesItems.add(new FoodItem("Nimbu Pani Fizzy",30));


        hotBeveragesItems.add(new FoodItem("Chai",18));
        hotBeveragesItems.add(new FoodItem("Cutting Chai",12));
        hotBeveragesItems.add(new FoodItem("Special Chai",30));
        hotBeveragesItems.add(new FoodItem("Cutting Special Chai",18));
        hotBeveragesItems.add(new FoodItem("Make My Chai",30));
        hotBeveragesItems.add(new FoodItem("Ginger Lemon Honey Tea",50));
        hotBeveragesItems.add(new FoodItem("Sugar Free Chai",30));
        hotBeveragesItems.add(new FoodItem("Black Chai(Lemon)",20));
        hotBeveragesItems.add(new FoodItem("Green Chai",30));
        hotBeveragesItems.add(new FoodItem("Hot Coffee",30));
        hotBeveragesItems.add(new FoodItem("Horlicks/ Bournvita",30));
        hotBeveragesItems.add(new FoodItem("Masala Milk",30));


        accompanimentsItems.add(new FoodItem("Khari",20));
        accompanimentsItems.add(new FoodItem("Extra Toppings",10));
        accompanimentsItems.add(new FoodItem("Mineral Water 500ml",10));
        accompanimentsItems.add(new FoodItem("Mineral Water 1000ml",20));


        snacksItems.add(new FoodItem("Bun Maska",35));
        snacksItems.add(new FoodItem("Bun Maska Jam",45));
        snacksItems.add(new FoodItem("Bun Cheese Maska",45));
        snacksItems.add(new FoodItem("Bun Maska Chocolate",45));
        snacksItems.add(new FoodItem("Bread Butter(Regular/ Toast)",30));
        snacksItems.add(new FoodItem("Bread Butter Jam(Regular/ Toast)",40));
        snacksItems.add(new FoodItem("Garlic Toast",25));
        snacksItems.add(new FoodItem("Chilli Toast",25));
        snacksItems.add(new FoodItem("Cheese Chilli Toast",35));
        snacksItems.add(new FoodItem("Cheese Garlic Toast",35));
        snacksItems.add(new FoodItem("Onion Toast",25));
        snacksItems.add(new FoodItem("Cheese Onion Toast",35));
        snacksItems.add(new FoodItem("Masala Maggi",30));
        snacksItems.add(new FoodItem("Butter Maggi",40));
        snacksItems.add(new FoodItem("Butter Cheese Maggi",50));
        snacksItems.add(new FoodItem("Butter Onion Maggi",50));
        snacksItems.add(new FoodItem("Butter Cheese Onion Maggi",60));


    }


    public static void clear(){
        foodItems.clear();   //Clearing Global List of FoodItem
        hotBeveragesItems.clear();
        coldBeveragesItems.clear();
        accompanimentsItems.clear();
        snacksItems.clear();
    }
   /* public static ArrayList<FoodItem> getColdBeveragesItems() {

        coldBeveragesItems.add(new FoodItem("Lemon Iced Chai",40));
        coldBeveragesItems.add(new FoodItem("Cold Coffee",40));
        coldBeveragesItems.add(new FoodItem("Nimbu Pani",20));
        coldBeveragesItems.add(new FoodItem("Nimbu Pani Fizzy",30));

        return coldBeveragesItems;
    }

    public static ArrayList<FoodItem> getHotBeveragesItems() {


        hotBeveragesItems.add(new FoodItem("Chai",18));
        hotBeveragesItems.add(new FoodItem("Cutting Chai",12));
        hotBeveragesItems.add(new FoodItem("Special Chai",30));
        hotBeveragesItems.add(new FoodItem("Cutting Special Chai",18));
        hotBeveragesItems.add(new FoodItem("Make My Chai",30));
        hotBeveragesItems.add(new FoodItem("Ginger Lemon Honey Tea",50));
        hotBeveragesItems.add(new FoodItem("Sugar Free Chai",30));
        hotBeveragesItems.add(new FoodItem("Black Chai(Lemon)",20));
        hotBeveragesItems.add(new FoodItem("Green Chai",30));
        hotBeveragesItems.add(new FoodItem("Hot Coffee",30));
        hotBeveragesItems.add(new FoodItem("Horlicks/ Bournvita",30));
        hotBeveragesItems.add(new FoodItem("Masala Milk",30));

        return hotBeveragesItems;
    }

    public static ArrayList<FoodItem> getAccompanimentsItems() {


        accompanimentsItems.add(new FoodItem("Khari",20));
        accompanimentsItems.add(new FoodItem("Extra Toppings",10));
        accompanimentsItems.add(new FoodItem("Mineral Water 500ml",10));
        accompanimentsItems.add(new FoodItem("Mineral Water 1000ml",20));

        return accompanimentsItems;
    }


    public static ArrayList<FoodItem> getSnacksItems() {

        snacksItems.add(new FoodItem("Bun Maska",35));
        snacksItems.add(new FoodItem("Bun Maska Jam",45));
        snacksItems.add(new FoodItem("Bun Cheese Maska",45));
        snacksItems.add(new FoodItem("Bun Maska Chocolate",45));
        snacksItems.add(new FoodItem("Bread Butter(Regular/ Toast)",30));
        snacksItems.add(new FoodItem("Bread Butter Jam(Regular/ Toast)",40));
        snacksItems.add(new FoodItem("Garlic Toast",25));
        snacksItems.add(new FoodItem("Chilli Toast",25));
        snacksItems.add(new FoodItem("Cheese Chilli Toast",35));
        snacksItems.add(new FoodItem("Cheese Garlic Toast",35));
        snacksItems.add(new FoodItem("Onion Toast",25));
        snacksItems.add(new FoodItem("Cheese Onion Toast",35));
        snacksItems.add(new FoodItem("Masala Maggi",30));
        snacksItems.add(new FoodItem("Butter Maggi",40));
        snacksItems.add(new FoodItem("Butter Cheese Maggi",50));
        snacksItems.add(new FoodItem("Butter Onion Maggi",50));
        snacksItems.add(new FoodItem("Butter Cheese Onion Maggi",60));


        return snacksItems;
    }*/
}
