package com.appcraftz.chaitapri;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;


public class HotBeverages extends AppCompatActivity {

   /* ListView listHot;*/
    RecyclerView rvHotBeverage;
    ColdBeveragesAdaptor adaptor;
    public ArrayList<FoodItem> hotbeveragesItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_hot_beverages);
    }


    @Override
    public void onStart() {
        super.onStart();
/*
if (GlobalData.hotBeveragesItems!=null)
        GlobalData.hotBeveragesItems.clear();
*/
        rvHotBeverage = (RecyclerView) findViewById(R.id.rvHotBeverage);
        hotbeveragesItems = GlobalData.hotBeveragesItems;
        adaptor = new ColdBeveragesAdaptor(GlobalData.hotBeveragesItems,HotBeverages.this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(HotBeverages.this,LinearLayoutManager.VERTICAL,false);
        rvHotBeverage.setLayoutManager(layoutManager);
        rvHotBeverage.setAdapter(adaptor);

     /*   listHot = (ListView) findViewById(R.id.liHot);
        //hotbeveragesItems = GlobalData.getHotBeveragesItems();

        adapter = new ListViewAdapter(GlobalData.hotBeveragesItems,getApplicationContext());

        listHot.setAdapter(adapter);
        adapter.setOnDisplayTotalCountClick(new DisplayTotalCountClick() {
            @Override
            public void onDisplayTotalCountClicked(int i) {

            }
        });
        adapter.notifyDataSetChanged();*/

    }

}
