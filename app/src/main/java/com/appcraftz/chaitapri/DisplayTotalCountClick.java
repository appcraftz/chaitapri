package com.appcraftz.chaitapri;

/**
 * Created by appcraftz on 23/9/17.
 */

public interface DisplayTotalCountClick {
    public void onDisplayTotalCountClicked(int i);
}
