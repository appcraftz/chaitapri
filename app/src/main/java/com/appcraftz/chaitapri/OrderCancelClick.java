package com.appcraftz.chaitapri;

/**
 * Created by appcraftz on 15/9/17.
 */

public interface OrderCancelClick {
    public void onOrderCancelClick(FoodItem item);
}
