package com.appcraftz.chaitapri;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.functions.Action1;

public class PlaceOrder extends AppCompatActivity {

    EditText name, contact, address;

    ProgressDialog dialog;
    UserData data;
    double distance;
    int total;
    Button order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        if (getIntent().getExtras() != null) {
            total = getIntent().getExtras().getInt("total");
           // Log.e("Total In Place Order", "" + total);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        dialog = new ProgressDialog(this);
        name = (EditText) findViewById(R.id.usrName);
        contact = (EditText) findViewById(R.id.usrContact);
        address = (EditText) findViewById(R.id.usrAddress);
        order = (Button) findViewById(R.id.requestOrder);
        // acquireLocation = (Button)findViewById(R.id.getLocation);

        if (!SPHelper.getSP(getApplicationContext(), "userName").equals("none") ||
                !SPHelper.getSP(getApplicationContext(), "userContact").equals("none") ||
                !SPHelper.getSP(getApplicationContext(), "userAddress").equals("none")) {

            name.setText(SPHelper.getSP(getApplicationContext(), "userName"));
            contact.setText(SPHelper.getSP(getApplicationContext(), "userContact"));
            address.setText(SPHelper.getSP(getApplicationContext(), "userAddress"));
        }
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!name.getText().toString().equals("") ||
                        !contact.getText().toString().equals("")||
                        !address.getText().toString().equals(""))
                {
                NetworkStatus networkstatus = NetworkStatus.getInstance(PlaceOrder.this);
                if (networkstatus.isOnline()) {
                    final Location location = new Location("");
                    location.setLatitude(19.9730944);
                    location.setLongitude(73.7849306);

//                  distance = location.distanceTo(new AppLocationService(PlaceOrder.this).getLocation(LocationManager.GPS_PROVIDER));

                    if (GlobalData.myLocation!=null) {
                        distance = location.distanceTo(GlobalData.myLocation);
                       // distance = 1000;

                    }else{  distance =5000; }
                 //   Log.e("BEtween2",""+(distance>3000));
                if(distance>1500)
                {
                    Toast.makeText(PlaceOrder.this,"You are outside 3Km Radius or your location is turned off. Unable to place order",Toast.LENGTH_SHORT).show();

                }


                SPHelper.setSP(getApplicationContext(),"userName",name.getText().toString());
                SPHelper.setSP(getApplicationContext(),"userContact",contact.getText().toString());
                SPHelper.setSP(getApplicationContext(),"userAddress",address.getText().toString());


                final ServerAgent agent = new ServerAgent(getApplicationContext());


                new AsyncTask<Void,Void,Void>(){
                    String resp;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                         data= new UserData(name.getText().toString(),contact.getText().toString(),address.getText().toString(),total);
                        dialog.setMessage("Placing Order please wait");
                        dialog.show();
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        resp = agent.placeOrder(data);

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        SPHelper.setSP(getApplicationContext(),"finish","true");
                        dialog.dismiss();
                        Intent intent = new Intent(PlaceOrder.this, OrderPlaced.class);
                        if (resp.equals("fail"))
                            intent.putExtra("orderPlaced",0);
                        else if (resp.equals("CONNECTION_ERROR"))
                            intent.putExtra("orderPlaced",1);
                        else
                            intent.putExtra("orderPlaced",2);
                        startActivity(intent);



                        finish();


                    }
                }.execute(null,null,null);


              //  }

                }else {
                    Toast.makeText(PlaceOrder.this, "You are not connected to Internet", Toast.LENGTH_SHORT).show();
                }


            }else{
                    Toast.makeText(PlaceOrder.this, "Empty Fields are not allowed..!", Toast.LENGTH_SHORT).show();
                }

            }

        });


        /*acquireLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppLocationService appLocationService = new AppLocationService(getApplicationContext());
                final Location location = appLocationService.getLocation(LocationManager.GPS_PROVIDER);
                if (location!= null) {
                    final String lat = String.valueOf(location.getLatitude());
                    final String lon = String.valueOf(location.getLongitude());

                    //Getting Latitude and Longitude
                }

                final ServerAgent agent = new ServerAgent(getApplicationContext());
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {

            // do something with the data to get address

                        return null;
                    }
                }.execute(null,null,null);
            }
        });*/
    }
}
