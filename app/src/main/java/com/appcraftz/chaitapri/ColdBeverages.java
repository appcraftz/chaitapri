package com.appcraftz.chaitapri;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class ColdBeverages extends AppCompatActivity {

   /* ListView listCold;
    ListViewAdapter adapter;*/
    ArrayList<FoodItem> coldbeveragesItems;
    RecyclerView rvColdBeverage;
    ColdBeveragesAdaptor adaptor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cold_beverages);
    }

    @Override
    public void onStart() {
        super.onStart();
     /*   if (coldbeveragesItems!=null)
        coldbeveragesItems.clear();
*/
        rvColdBeverage = (RecyclerView) findViewById(R.id.rvColdBeverage);
        coldbeveragesItems = GlobalData.coldBeveragesItems;
        adaptor = new ColdBeveragesAdaptor(GlobalData.coldBeveragesItems,ColdBeverages.this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        rvColdBeverage.setLayoutManager(layoutManager);
        rvColdBeverage.setAdapter(adaptor);

       /* listCold = (ListView)findViewById(R.id.liCold);
        //coldbeveragesItems = GlobalData.getColdBeveragesItems();


        adapter = new ListViewAdapter(GlobalData.coldBeveragesItems,getApplicationContext());

        listCold.setAdapter(adapter);

        adapter.setOnDisplayTotalCountClick(new DisplayTotalCountClick() {
            @Override
            public void onDisplayTotalCountClicked(int i) {

            }
        });
        adapter.notifyDataSetChanged();*/

    }
}
