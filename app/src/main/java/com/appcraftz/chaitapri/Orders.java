package com.appcraftz.chaitapri;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Orders extends AppCompatActivity {

    ListView liOrders;
    OrderAdapter adapter;
    ArrayList<FoodItem> foodItemArrayList;
    TextView totalPrice;
    Button placeOrder,calculateTotal;
    static int wholeTotal = 0;
    int sendTotal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

     /*   for (FoodItem item: GlobalData.foodItems){
            item.setQuantity(1);
        }
        foodItemArrayList = GlobalData.foodItems;*/

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (SPHelper.getSP(getApplicationContext(),"finish").equals("true"))
            finish();
        totalPrice = (TextView)findViewById(R.id.wholeTotal);
        placeOrder = (Button)findViewById(R.id.placeOrder);
       // calculateTotal = (Button)findViewById(R.id.calculate);

        liOrders = (ListView)findViewById(R.id.liOrders);


        adapter = new OrderAdapter(Cart.foodItems,this);

        liOrders.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        adapter.setOnOrderCancelClick(new OrderCancelClick() {
            @Override
            public void onOrderCancelClick(FoodItem item) {

                Cart.removeFromCart(item);
                adapter.notifyDataSetChanged();
            }

        });

      /*  calculateTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (FoodItem item: Cart.getCartItems()){
                    wholeTotal = wholeTotal+item.getTotal();
                }

                sendTotal = wholeTotal;
                String tot = String.valueOf(wholeTotal);
                totalPrice.setText(tot);
                wholeTotal =0;


            }
        });
*/
          int tPrice =0 ;
          for (FoodItem item: Cart.getCartItems())
                tPrice = tPrice + item.getPrice();
        //  Log.e("Total",""+tPrice);
          totalPrice.setText(""+tPrice);



        adapter.setOnDisplayQuantityClick(new DisplayQuantityClick() {
            @Override
            public void onDisplayquantityClick(ArrayList<FoodItem> foodItemArrayList) {
                for (FoodItem item: Cart.getCartItems()){
                    wholeTotal = wholeTotal+item.getTotal();
                }
                sendTotal = wholeTotal;
                String tot = String.valueOf(wholeTotal);
                totalPrice.setText(tot);
                wholeTotal =0;

            }
        });



       placeOrder.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (sendTotal >=100) {
                   for (FoodItem item: Cart.getCartItems()){
            item.setOrdered(false);
        }
                   Intent intent = new Intent(Orders.this, PlaceOrder.class);
              //     Log.e("Whole Total in ", "Orders" + sendTotal);
                   intent.putExtra("total", sendTotal);
                   startActivity(intent);


               }else {
                   Toast.makeText(Orders.this, "Minimum Total must be greater than 100..!", Toast.LENGTH_SHORT).show();
               }
           }
       });


//        ListViewSize.setListViewHeightBasedOnChildren(liOrders);
    }


}
