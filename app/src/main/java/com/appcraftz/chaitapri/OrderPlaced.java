package com.appcraftz.chaitapri;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OrderPlaced extends AppCompatActivity {

    int orderPlaced;
    TextView msg;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);

        if (getIntent().getExtras()!=null){
            orderPlaced = getIntent().getExtras().getInt("orderPlaced");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        msg = (TextView)findViewById(R.id.orderMessage);
        fab = (FloatingActionButton)findViewById(R.id.home);

        if (orderPlaced == 0)
            msg.setText("Your Order Cannot be Placed due to Poor Internet Connection...");
        else if (orderPlaced == 1)
            msg.setText("Your Order Cannot be placed please try again...");
        else if (orderPlaced == 2)
            msg.setText("Your Order has been Placed...");
        else
            msg.setText("Contact Administrator");


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cart.ClearCart();
                startActivity(new Intent(OrderPlaced.this,MainActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(OrderPlaced.this,MainActivity.class));
        Cart.ClearCart();
        finish();
    }
}
