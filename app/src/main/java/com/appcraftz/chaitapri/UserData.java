package com.appcraftz.chaitapri;

/**
 * Created by appcraftz on 15/9/17.
 */

public class UserData {
    String name;
    String contact;
    String address;
    int total;

    UserData(String name, String contact, String address,int total){
        this.name = name;
        this.contact = contact;
        this.address = address;
        this.total = total;
    }


    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
