package com.appcraftz.chaitapri;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by appcraftz on 13/9/17.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;


    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
      /*  if (position == 0)
            return new HotBeverages();*/
       /* else if (position == 1)
            return new ColdBeverages();*/
        /* if (position==2)
            return new Accompaniments();*/
       /* else
            return new Snacks();*/
       return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Hot Beverages";

            case 1:
                return "Cold Beverages";
            case 2:
                return "Accompaniments";
            case 3:
                return "Snacks";
            default:
                return null;
        }
    }
}
