package com.appcraftz.chaitapri;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by appcraftz on 15/9/17.
 */

public class ListViewAdapter extends BaseAdapter{

    ArrayList<FoodItem> foodItemArrayList;
    Context context;
    LayoutInflater inflater;
    FoodItem item;
    DisplayTotalCountClick displayTotalCountClick;

    int position;

    ListViewAdapter(ArrayList<FoodItem> foodItemArrayList, Context context){

        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.foodItemArrayList = foodItemArrayList;
    }

    @Override
    public int getCount() {
        if (foodItemArrayList!=null)
            return foodItemArrayList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        return foodItemArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

  /*      final ViewHolder holder;
        View v = view;



        if (v == null){
            v = inflater.inflate(R.layout.fooditem,viewGroup,false);

            holder = new ViewHolder();

            holder.itemName = (TextView) v.findViewById(R.id.itemName);
            holder.itemPrice = (TextView)v.findViewById(R.id.itemPrice);
            holder.addToCart = (ImageButton)v.findViewById(R.id.addToCart);

            v.setTag(holder);
        }else{
            holder = (ViewHolder) v.getTag();
        }



     position= i;

        holder.itemName.setText(foodItemArrayList.get(i).getName());
        holder.itemPrice.setText(String.valueOf(foodItemArrayList.get(i).getPrice()));

        //  Setting Button Background Selected if getOrdered is true
        if (foodItemArrayList.get(position).getOrdered())
                holder.addToCart.setBackgroundResource(R.drawable.rect3);

        item = new FoodItem(foodItemArrayList.get(position).getName(),foodItemArrayList.get(position).getPrice());
        holder.addToCart.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {

                if (foodItemArrayList.get(position).getOrdered()){
                    Log.e("Already Added","sad");
                   // holder.addToCart.setBackgroundResource(R.drawable.rect3);
                }
                else {
                    Log.e("Adding","Yup");
                    GlobalData.foodItems.add(new FoodItem(foodItemArrayList.get(position).getName(), foodItemArrayList.get(position).getPrice()));
                    holder.addToCart.setBackgroundResource(R.drawable.rect3);
                    foodItemArrayList.get(position).setOrdered(true);
                }


                if (GlobalData.foodItems!=null) {
                    Log.e("/n*****","FoodItems");
                    for (FoodItem items : GlobalData.foodItems) {

                        Log.e("ItemN", items.getName());
                        Log.e("ItemP", "" + items.getPrice());
                    }
                }
            }
        });

*/

        View v = inflater.inflate(R.layout.fooditem,null);

        final int position = i;

        TextView itemName = (TextView) v.findViewById(R.id.itemName);
        TextView itemPrice = (TextView)v.findViewById(R.id.itemPrice);
        itemName.setText(foodItemArrayList.get(i).getName());
        itemPrice.setText(String.valueOf(foodItemArrayList.get(i).getPrice()));
        final ImageButton addToCart = (ImageButton)v.findViewById(R.id.addToCart);

    //  Setting Button Background Selected if getOrdered is true
        if (foodItemArrayList.get(position).getOrdered())
           addToCart.setBackgroundResource(R.drawable.rect3);

        item = new FoodItem(foodItemArrayList.get(position).getName(),foodItemArrayList.get(position).getPrice());
        addToCart.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {

                if (foodItemArrayList.get(position).getOrdered()){
                  // Log.e("Already Added","sad");
                    addToCart.setBackgroundResource(R.drawable.rect3);
                }
                else {
                    //Log.e("Adding","Yup");
                    GlobalData.foodItems.add(new FoodItem(foodItemArrayList.get(position).getName(), foodItemArrayList.get(position).getPrice()));
//                    GlobalData.foodItems.get(GlobalData.foodItems.size()-1).setQuantity(1);
                    addToCart.setBackgroundResource(R.drawable.rect3);
                    foodItemArrayList.get(position).setOrdered(true);
                    if (displayTotalCountClick!=null){

                        ((MainActivity)context).globalCount();
                    }
                    SPHelper.setSP(context,"size",""+GlobalData.foodItems.size());
                }


                /*if (GlobalData.foodItems!=null) {
                    Log.e("/n*****","FoodItems");
                    for (FoodItem items : GlobalData.foodItems) {

                        Log.e("ItemN", items.getName());
                        Log.e("ItemP", "" + items.getPrice());
                    }
                }*/
            }
        });

        return v;
    }

    private class ViewHolder{
        TextView itemName,itemPrice;
        ImageButton addToCart;
    }


    public void setOnDisplayTotalCountClick(DisplayTotalCountClick displayTotalCountClick){
        this.displayTotalCountClick = displayTotalCountClick;
    }
}
